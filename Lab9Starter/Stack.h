#pragma once
template<class T>class Stack
{
private:
	StackNode<T> * makeNode(T data);
	StackNode<T> * root;
	void DestructorHelper(StackNode<T> * curNode); // recursive
	void CopyHelper(StackNode<T> * curNode); //recursive
public:
	T Pop();
	Stack();
	void PrintStack();
	void Push(T data);
	Stack(Stack<T> const &original);
	~Stack();
};

